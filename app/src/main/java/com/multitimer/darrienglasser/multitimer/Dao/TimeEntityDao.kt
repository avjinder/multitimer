package com.multitimer.darrienglasser.multitimer.Dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.multitimer.darrienglasser.multitimer.Entity.TimeEntity

@Dao
interface TimeEntityDao {
    @Query("SELECT id FROM TimeEntity WHERE NOT complete")
    fun getActiveId(): List<Int>

    @Query("SELECT * FROM TimeEntity WHERE id = (:id)")
    fun getEntity(id: Int): TimeEntity

    @Query("SELECT * FROM TimeEntity WHERE complete ORDER BY endDate desc")
    fun getCompleteTimes(): MutableList<TimeEntity>

    @Query("UPDATE TimeEntity SET delta = (:delta), saveTime = (:saveTime) WHERE id = (:id)")
    fun updateDelta(id: Int, delta: Long, saveTime: Long)

    @Query("UPDATE TimeEntity SET description = (:description) where id = (:id)")
    fun updateDescription(id: Int, description: String)

    @Query("UPDATE TimeEntity SET complete = (:complete), endDate = (:endDate) WHERE id = (:id)")
    fun markComplete(
        id: Int,
        complete: Boolean = true,
        endDate: Long = (System.currentTimeMillis() / 1000L)
    )

    @Query("UPDATE TimeEntity SET running = (:running) WHERE id = (:id)")
    fun setRunning(id: Int, running: Boolean)

    @Insert
    fun newTime(entity: TimeEntity): Long
}

