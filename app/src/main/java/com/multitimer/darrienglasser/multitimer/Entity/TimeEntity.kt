package com.multitimer.darrienglasser.multitimer.Entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class TimeEntity(
    @PrimaryKey(autoGenerate = true) var id: Int?,
    @ColumnInfo(name = "delta") var delta: Long,
    @ColumnInfo(name = "description") var description: String,
    @ColumnInfo(name = "saveTime") var saveTime: Long,
    @ColumnInfo(name = "endDate") var endDate: Long,
    @ColumnInfo(name = "running") var running: Boolean,
    @ColumnInfo(name = "complete") var complete: Boolean) {

    fun trueDelta(): Long {
       return if (running && !complete) {
            delta + (System.currentTimeMillis() / 1000L - saveTime)
        } else {
            delta
        }
    }
}

