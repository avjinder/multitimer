package com.multitimer.darrienglasser.multitimer.Activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.multitimer.darrienglasser.multitimer.Database.DbConnector
import com.multitimer.darrienglasser.multitimer.Fragments.ClockListFragment
import com.multitimer.darrienglasser.multitimer.Fragments.PageFragment
import com.multitimer.darrienglasser.multitimer.Fragments.TimerHistoryFragment
import com.multitimer.darrienglasser.multitimer.R
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        GlobalScope.launch {
            DbConnector(applicationContext)
        }

        val tabs = listOf(
            ClockListFragment(),
            TimerHistoryFragment()
        )
        tabs.forEach { tab_view.newTab().text = it.getTitle() }
        val pager = FragmentPager(
            supportFragmentManager,
            tabs
        )
        main_pager.adapter = pager
    }
}

class FragmentPager(
    fm: androidx.fragment.app.FragmentManager,
    private val pages: List<PageFragment>
) : androidx.fragment.app.FragmentStatePagerAdapter(fm) {

    override fun getCount(): Int  = pages.size

    override fun getItem(position: Int): androidx.fragment.app.Fragment {
        return pages[position]
    }

    override fun getPageTitle(position: Int): CharSequence {
        return pages[position].getTitle()
    }
}

