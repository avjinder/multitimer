package com.multitimer.darrienglasser.multitimer.Fragments

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.multitimer.darrienglasser.multitimer.R

private const val TASK_DESCRIPTION = "taskDescription"

class ConfirmDeletionFragment : DialogFragment() {
    private var taskDescription: String? = null
    var choiceListener: ((Boolean) -> Unit)? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            taskDescription = it.getString(TASK_DESCRIPTION)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_confirm_deletion, container, false)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val title = context?.getString(R.string.confirm_deletion)
        val bundledText = arguments?.getString(TASK_DESCRIPTION)
        val confirmText =  if (bundledText.isNullOrEmpty()) "this timer" else bundledText

        val alertDialogBuilder = AlertDialog.Builder(context!!)
        alertDialogBuilder.setTitle(title)
        alertDialogBuilder.setMessage(
            String.format(
                context?.getString(R.string.confirmation_text) ?: "Android is corrupted :(",
                confirmText))

        alertDialogBuilder.setPositiveButton("YES") { _, _ ->
            choiceListener?.invoke(true)
            dialog?.dismiss()
        }

        alertDialogBuilder.setNegativeButton("NO") { _, _ ->
            choiceListener?.invoke(false)
            dialog?.dismiss()
        }

        return alertDialogBuilder.create()
    }

    companion object {
        @JvmStatic
        fun newInstance(taskDescription: String) =
            ConfirmDeletionFragment().apply {
                arguments = Bundle().apply {
                    putString(TASK_DESCRIPTION, taskDescription)
                }
            }
    }
}
