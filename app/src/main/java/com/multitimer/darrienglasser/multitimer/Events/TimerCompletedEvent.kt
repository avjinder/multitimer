package com.multitimer.darrienglasser.multitimer.Events

import com.multitimer.darrienglasser.multitimer.Entity.TimeEntity

class TimerCompletedEvent(val entity: TimeEntity)