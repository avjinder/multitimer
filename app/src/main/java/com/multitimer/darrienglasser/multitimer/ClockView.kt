package com.multitimer.darrienglasser.multitimer

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.annotation.UiThread
import androidx.appcompat.app.AppCompatActivity
import com.multitimer.darrienglasser.multitimer.Database.DbConnector
import com.multitimer.darrienglasser.multitimer.Entity.TimeEntity
import com.multitimer.darrienglasser.multitimer.Events.TimerCompletedEvent
import com.multitimer.darrienglasser.multitimer.Fragments.ConfirmDeletionFragment
import com.multitimer.darrienglasser.multitimer.Fragments.EditDescriptionFragment
import kotlinx.android.synthetic.main.clock_view.view.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus

class ClockView : LinearLayout {
    @Volatile
    private lateinit var timeEntity: TimeEntity

    /**
     * In the case that the user pauses a clock and leaves the app,
     * and the app is later killed, we cannot trust the save time.
     *
     * It is possible the save time is not being updated while the app
     * is in the background.
     *
     * In the case the user unpauses before the save time is updated,
     * the time on the clock is corrupted.
     *
     * In the case of a paused clock, the delta is the only source of
     * truth.
     */
    private var trustSaveTime = true

    constructor(context: Context) : super(context) {
        init(null, 0)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        init(attrs, defStyle)
    }

    private fun init(attrs: AttributeSet?, defStyle: Int) {
        context.obtainStyledAttributes(
            attrs, R.styleable.ClockView, defStyle, 0
        ).apply {
            recycle()
        }

        val inflater = context
            .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        inflater.inflate(R.layout.clock_view, this, true)
    }

    public fun setTimerId(timerId: Int) {
        GlobalScope.launch {
            timeEntity = DbConnector.db.timeDao().getEntity(timerId)
            onEntityInitialized()
        }
    }

    @UiThread
    private fun onEntityInitialized() {
        Handler(Looper.getMainLooper()).post {
            val fm = (context as AppCompatActivity).supportFragmentManager
            delete.setOnClickListener {
                val confirmDeletionDialog = ConfirmDeletionFragment.newInstance(timeEntity.description)

                confirmDeletionDialog.choiceListener = { choice ->
                    if (choice) {
                        GlobalScope.launch {
                            DbConnector.db.timeDao().markComplete(timeEntity.id ?: -1)
                            timeEntity.endDate = (System.currentTimeMillis() / 1000L)
                            EventBus.getDefault().post(TimerCompletedEvent(timeEntity))
                        }

                    }
                }
                confirmDeletionDialog.show(fm, "confirm deletion")
            }

            description_view.setOnClickListener {
                val changeDescriptionDialog = EditDescriptionFragment.newInstance(timeEntity.description)
                changeDescriptionDialog.choiceListener = {choice, text ->
                    changeDescriptionDialog.dismiss()
                    if (choice) {
                        changeDescriptionDialog.dismiss()
                        timeEntity.description = text
                        description_text.text = text
                        GlobalScope.launch {
                            DbConnector.db.timeDao().updateDescription(
                                timeEntity.id ?: -1,
                                timeEntity.description
                            )
                        }
                    }
                }
                changeDescriptionDialog.show(fm, "edit")
            }

            pause_play_button.setOnClickListener {
                setRunning(!timeEntity.running)
            }

            setRunning(timeEntity.running)
            timeWatcher()
        }
    }

    private fun timeWatcher() {
        GlobalScope.launch {
            while (true) {
                updateClock()
                delay(1000)
                if (timeEntity.running) {
                    // What exactly is this doing? It gets the true delta!
                    // But Darrien, where do you increment the timer? Won't this always
                    // return the same number?
                    // Ah no my future maintainer (or Darrien), check out the trueDelta method.
                    // You'll note we subtract the current time from the save time to get the delta
                    // and what do you know? A second tends to have passed, so we don't need to increment.
                    // On an unrelated note, fuck clocks.
                    timeEntity.delta = timeEntity.trueDelta()
                }
                timeEntity.saveTime = (System.currentTimeMillis() / 1000L)
                DbConnector.db.timeDao().updateDelta(
                    timeEntity.id ?: -1,
                    timeEntity.delta,
                    timeEntity.saveTime
                )
            }
        }
    }

    @UiThread
    private fun updateClock() {
        Handler(Looper.getMainLooper()).post {
            val minutes = timeEntity.trueDelta() / 60
            val seconds = timeEntity.trueDelta() % 60
            val sSeconds = if (seconds < 10) "0$seconds" else "$seconds"
            val time = "$minutes:$sSeconds"
            clock_time.text = time
            if (timeEntity.description.isNotEmpty()) {
                description_text.text = timeEntity.description
            }
        }
    }

    private fun setRunning(isRunning: Boolean) {
        timeEntity.running = isRunning
        if (timeEntity.running) {
            pause_play_button.setImageResource(R.drawable.ic_pause)
        } else {
            pause_play_button.setImageResource(R.drawable.ic_play)
        }

        GlobalScope.launch {
            DbConnector.db.timeDao().setRunning(timeEntity.id ?: -1, timeEntity.running)
        }
    }
}
