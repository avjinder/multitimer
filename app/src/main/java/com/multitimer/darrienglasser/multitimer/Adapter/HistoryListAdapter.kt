package com.multitimer.darrienglasser.multitimer.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.multitimer.darrienglasser.multitimer.Entity.TimeEntity
import com.multitimer.darrienglasser.multitimer.R
import java.text.SimpleDateFormat
import java.util.Locale

class HistoryListAdapter(
    private val inactiveTimes: MutableList<TimeEntity>,
    private val context: Context
): RecyclerView.Adapter<HistoryListAdapter.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val taskView: TextView = itemView.findViewById(R.id.task_view)
        val dateView: TextView = itemView.findViewById(R.id.date_view)
        val durationView: TextView = itemView.findViewById(R.id.duration_view)
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryListAdapter.ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.history_row, parent, false))
    }

    override fun getItemCount(): Int = inactiveTimes.size

    override fun onBindViewHolder(holder: HistoryListAdapter.ViewHolder, position: Int) {
        val timeEntity = inactiveTimes[position]
        if (timeEntity.description.isNotEmpty()) {
            holder.taskView.text = timeEntity.description
        }

        holder.durationView.text = String.format(
            context.getString(R.string.duration_string),
            gibeTimeString(timeEntity)
        )

        holder.dateView.text = String.format(
            context.getString(R.string.date_string),
            gibeDateString(timeEntity)
        )
    }

    private fun gibeTimeString(timeEntity: TimeEntity): String {
        val minutes = timeEntity.trueDelta() / 60
        val seconds = timeEntity.trueDelta() % 60
        val sSeconds = if (seconds < 10) "0$seconds" else "$seconds"

        return "$minutes:$sSeconds"
    }

    private fun gibeDateString(timeEntity: TimeEntity): String {
        val time = java.util.Date(timeEntity.endDate * 1000)

        val formatter = SimpleDateFormat("MM/dd/yy", Locale.getDefault())
        return formatter.format(time)
    }

    fun addEntity(timeEntity: TimeEntity) {
        inactiveTimes.add(0, timeEntity)
        notifyDataSetChanged()
    }
}
