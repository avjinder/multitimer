package com.multitimer.darrienglasser.multitimer.Fragments

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.UiThread
import androidx.recyclerview.widget.LinearLayoutManager
import com.multitimer.darrienglasser.multitimer.Adapter.HistoryListAdapter
import com.multitimer.darrienglasser.multitimer.Database.DbConnector
import com.multitimer.darrienglasser.multitimer.Entity.TimeEntity
import com.multitimer.darrienglasser.multitimer.Events.TimerCompletedEvent
import com.multitimer.darrienglasser.multitimer.R
import kotlinx.android.synthetic.main.fragment_timer_history.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class TimerHistoryFragment : PageFragment() {
    override fun getTitle(): String = "Timer History"
    lateinit var adapter: HistoryListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        EventBus.getDefault().register(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_timer_history, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
    }

    override fun onDetach() {
        super.onDetach()
        EventBus.getDefault().unregister(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        GlobalScope.launch {
            setupList(DbConnector.db.timeDao().getCompleteTimes())
        }
    }

    @UiThread
    fun setupList(timeEntities: MutableList<TimeEntity>) {
        Handler(Looper.getMainLooper()).post {
            if (timeEntities.isEmpty()) no_history.visibility = View.VISIBLE
            adapter = HistoryListAdapter(timeEntities, context!!)
            history_view.layoutManager = LinearLayoutManager(context)
            history_view.adapter = adapter
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun timerCompleted(timerCompletedEvent: TimerCompletedEvent) {
        adapter.addEntity(timerCompletedEvent.entity)
        no_history.visibility = View.GONE
    }
}
