package com.multitimer.darrienglasser.multitimer.Adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.multitimer.darrienglasser.multitimer.ClockView
import com.multitimer.darrienglasser.multitimer.R

class ClockListAdapter(private val ids: MutableList<Int>) : RecyclerView.Adapter<ClockListAdapter.ViewHolder>() {
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var clockView: ClockView = itemView.findViewById(R.id.clock_list_views)
    }

    override fun getItemCount(): Int = ids.size

    override fun onBindViewHolder(holder: ClockListAdapter.ViewHolder, position: Int) {
        holder.clockView.setTimerId(ids[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ClockListAdapter.ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.clock_list_row, parent, false))
    }

    fun addTimer(timerId: Int) {
        ids.add(timerId)
        notifyItemInserted(ids.size - 1)
    }

    fun remove(id: Int?) {
        if (id == null) throw RuntimeException("Null ID. Database is corrupted.")
        val position = ids.indexOf(id)
        ids.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, ids.size)
    }
}
