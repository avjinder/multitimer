package com.multitimer.darrienglasser.multitimer.Fragments

import com.multitimer.darrienglasser.multitimer.Views.Page

abstract class PageFragment : androidx.fragment.app.Fragment(), Page
