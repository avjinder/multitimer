package com.multitimer.darrienglasser.multitimer.Views

interface Page {
    fun getTitle(): String
}

